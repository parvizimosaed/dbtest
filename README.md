# Git Update
To update code, first of all, we should make a repository in git lab, and then open git bash in that directory:<br/>
1) Initialize the git:<br/>
`git init`<br/>
2) Connect via remote access <br/>
`git remote add origin git@gitlab.com:parvizimosaed/dbtest.git`<br/>
3) Add all files <br/>
`git add .`<br/>
4) Commit those files to git <br/>
`git commit -m '1st commit'`<br/>
5) Push those files if it works <br/>
`git push -u origin master`<br/>
6) if it does not work, use this ssh <br/>
`git pull --rebase origin master`
7) Pull from the git <br/>
`git pull origin master`
<br/>
To see the files in git, we can use this script: `ls -la`<br/>

# Database Backup
To get the database from the server, we should use these script sequentially <br/>
*  On the server `docker exec -i iciproject_db_1 pg_dump -U ici_user ici_db > backup.sql`
*  On local system `scp alireza@ptrack.edgecomenergy.ca:/home/alireza/iciproject/backup.sql`
*  On local system `docker exec -i iciproject_db_1 psql -U ici_user ici_db < backup.sql`

# Code Deployment
To get the code for the main dashboard, we should run these code on local system:
* on git for main dashboard `git pull origin master`
* on git for test dashboard `git checkout dev`, and then `git pull gitlab dev`
* on pycharm `docker-compose up --build` 

Then check everything on the localhost:1337. Also, for the deployment we have to run these codes  <br/>
* for main dashboard `./deploy.sh prod`
* for test dashboard `./deploy.sh dev` <br/>
Other scripts for gits:
* change the branch `git checkout master`
* remove the branch `git remote rm {branch name} `

# Docker Configuration
* docker stop `docker stop $(dockername)`
* docker down `docker-compose down`
* docker status `docker ps`
* docker compose build `docker-compose up --build`

# Download file from Server
* on local system 'scp alireza@ptrack.edgecomenergy.ca:/home/alireza/ptrack_ml_models/prediction/prediction_results.csv .'

# How to connect server
* Putty: Just connect to it via username and password
* ssh: ssh alireza@ptrack.edgecomenergy.ca 
